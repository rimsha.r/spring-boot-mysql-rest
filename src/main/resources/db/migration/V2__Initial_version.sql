CREATE TABLE `address_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `business` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_stc9bmtv2904s004l712f6lh3` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `phone_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_40y3cjv1bwwjwm4jxbiy1dosb` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `pax` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK4gpwct9lol0cmjhyyk8dqn334` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `pax_addresses` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passenger_id` bigint(20) NOT NULL,
  `address_type_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK6rpwummisfu2lahyy2m6vngdk` (`passenger_id`,`address_type_id`),
  KEY `FK5os3joes5kpudgsx0yjpf3lau` (`address_type_id`),
  CONSTRAINT `FK47eacd8mpem8dbpvvv3qhp50w` FOREIGN KEY (`passenger_id`) REFERENCES `pax` (`id`),
  CONSTRAINT `FK5os3joes5kpudgsx0yjpf3lau` FOREIGN KEY (`address_type_id`) REFERENCES `address_types` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `pax_login` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passenger_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_k4xndn4jlbwb1psdiqn0xcn55` (`passenger_id`),
  UNIQUE KEY `UKb7dll2s3coey99ntsbj5b2oi4` (`user_name`),
  CONSTRAINT `FKs60vs1roaotinv196ws52gvob` FOREIGN KEY (`passenger_id`) REFERENCES `pax` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `pax_name_data` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passenger_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_rmyrruwqbp341wa9y6i79ut4l` (`passenger_id`),
  CONSTRAINT `FK1uv57cng1eseq3okg8bojjlqq` FOREIGN KEY (`passenger_id`) REFERENCES `pax` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `pax_phones` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dialing_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passenger_id` bigint(20) NOT NULL,
  `phone_type_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKmhsupqcpa29bn9s1d9h4eg414` (`passenger_id`,`phone_type_id`),
  KEY `FK3itbygsl6lmpfwkkb3o26ycwp` (`phone_type_id`),
  CONSTRAINT `FK3itbygsl6lmpfwkkb3o26ycwp` FOREIGN KEY (`phone_type_id`) REFERENCES `phone_types` (`id`),
  CONSTRAINT `FKhmc9ob29b30a04bl1aik7rh8k` FOREIGN KEY (`passenger_id`) REFERENCES `pax` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;