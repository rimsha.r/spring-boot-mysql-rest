CREATE TABLE passenger_visited_countries (
  passenger_id bigint NOT NULL,
  country_id bigint NOT NULL,
  PRIMARY KEY (passenger_id,country_id),
  KEY FK74vev1isld13mlwus2qgsd4o (country_id),
  CONSTRAINT FK74vev1isld13mlwus2qgsd4o FOREIGN KEY (country_id) REFERENCES countries (id),
  CONSTRAINT FK8ogwkjdal42covtqjfy6fredn FOREIGN KEY (passenger_id) REFERENCES pax (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;