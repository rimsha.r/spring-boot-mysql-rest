package com.example.mysqlRest.model.pax;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "pax_login",
		uniqueConstraints = @UniqueConstraint(columnNames = {"user_name"}))
@Data
@EqualsAndHashCode(callSuper=true)
public class PaxLogin extends OneToOneLinkedToPassenger {
	@NotEmpty
	@Column(name = "user_name")
	private String userName;
	@NotEmpty
	private String password;
}
