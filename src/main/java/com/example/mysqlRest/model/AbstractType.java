package com.example.mysqlRest.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotEmpty;

@MappedSuperclass
@Getter@Setter
public abstract class AbstractType extends AbstractModel {
	@NotEmpty
	@Column(unique = true)
	protected String code;
	@NotEmpty
	protected String name;

	@Override
	public String toString() {
		return name;
	}
}
