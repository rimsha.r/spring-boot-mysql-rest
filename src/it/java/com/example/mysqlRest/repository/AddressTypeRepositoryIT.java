package com.example.mysqlRest.repository;

import com.example.mysqlRest.model.AddressType;
import com.example.mysqlRest.model.enums.Gender;
import com.example.mysqlRest.model.pax.Passenger;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.example.mysqlRest.model.enums.Gender.F;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
class AddressTypeRepositoryIT {
    @Autowired
    AddressTypeRepository addressTypeRepository;

    @Autowired
    PassengerRepository passengerRepository;

    @Test
    void findByCode() {
        AddressType h = addressTypeRepository.findByCode("H");
        log.info(h.getName());
        assertEquals("H", h.getCode());
    }

    @Test
    @Transactional
    void findAddressTypeCode(){
        List<Passenger> passengerList = passengerRepository.findAddressTypeCode("H");
        log.info("" + passengerList.size());
        assertEquals(381, passengerList.size());
    }

    @Test
    @Transactional
    void findGender(){
        List<Passenger> femaleList = passengerRepository.findGender(F);
        log.info("" + femaleList.size());
        assertTrue(femaleList.get(0).getGender() == F);
    }
}