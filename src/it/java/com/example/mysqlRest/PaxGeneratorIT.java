package com.example.mysqlRest;

import com.example.mysqlRest.model.AddressType;
import com.example.mysqlRest.model.enums.Gender;
import com.example.mysqlRest.model.enums.PaxStatus;
import com.example.mysqlRest.model.enums.Title;
import com.example.mysqlRest.model.pax.Passenger;
import com.example.mysqlRest.model.pax.PaxAddress;
import com.example.mysqlRest.model.pax.PaxName;
import com.example.mysqlRest.repository.AddressTypeRepository;
import com.example.mysqlRest.repository.PassengerRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.IntStream;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
class PaxGeneratorIT {
    @Autowired
    PassengerRepository passengerRepository;
    @Autowired
    AddressTypeRepository addressTypeRepository;

    List<AddressType> allAddressTypes;
    Random random;

    @BeforeEach
    void beforeAll() {
       allAddressTypes = addressTypeRepository.findAll();
       random = new Random();
    }

    @Test
    void generate() {
        List<Passenger> paxList = new ArrayList<>();

        IntStream.range(0, 500)
                .forEach(i -> paxList.add(createRandomPassenger()));

        passengerRepository.saveAll(paxList);
    }

    Passenger createRandomPassenger() {
        Passenger pax = createPassengerBase();

        PaxName name = createPaxName();
        pax.setName(name);
        name.setPassenger(pax);

        int addressesCount = random.nextInt(allAddressTypes.size());
        log.info("Address count: " + addressesCount);
        for (int i=0; i < addressesCount; i++) {
            PaxAddress address = createPaxAddress(i);
            pax.getAddresses().add(address);
            address.setPassenger(pax);
        }

        return pax;
    }

    Passenger createPassengerBase() {
        Passenger pax = new Passenger();
        pax.setStatus(randomPaxStatus());
        UUID uuid = UUID.randomUUID();
        pax.setEmail(uuid.toString() + "@gmail.com");
        pax.setGender(randomGender());
        return pax;
    }

    PaxName createPaxName() {
        PaxName name = new PaxName();
        name.setFirstName("First" + getAlphaNumericString(7));
        name.setLastName("Last" + getAlphaNumericString(7));
        name.setTitle(randomTitle());

        return name;
    }

    PaxAddress createPaxAddress(int index) {
        PaxAddress address = new PaxAddress();
        address.setCity("City" + getAlphaNumericString(7));
        address.setCountry("Latvia");
        address.setPostcode("LV-88" + random.nextInt(200));
        address.setStreet("Street " + random.nextInt(400));
        address.setType(allAddressTypes.get(index));

        return address;
    }

    Title randomTitle(){
        return Title.values()[random.nextInt(Title.values().length)];
    }

    PaxStatus randomPaxStatus(){
        return PaxStatus.values()[random.nextInt(PaxStatus.values().length)];
    }

    Gender randomGender(){
        return Gender.values()[random.nextInt(Gender.values().length)];
    }

    String getAlphaNumericString(int n){
        String AlphaNumericString = "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {
            int index
                    = (int)(AlphaNumericString.length()
                    * Math.random());
            sb.append(AlphaNumericString
                    .charAt(index));
        }
        return sb.toString();
    }
}